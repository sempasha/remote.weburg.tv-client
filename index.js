/* jshint ignore:start */
/**
 * @external jsonrpc
 * @desc Реализация клиент-серверного взаимодействия по протоколу
 * [JSON-RPC 2.0]{@link http://www.jsonrpc.org/specification} для JavaScript.
 * Не имеет реализации транспорта сообщений, для реализации отправки/получения JSON-RPC сообщений заложен интерфейс и
 * асбтрактные методы.
 * @see [jsonrpc]{@link https://bitbucket.org/sempasha/node-jsonrpc-sockjs-browser}
 */

/**
 * @class external:jsonrpc.Error
 * @desc Класс, описывающий ошибки по протоколу JSON-RPC, в отличие от обычных ошибок так же имеют код
 * (поле <code>code</code>) и могут иметь дополнительные данные (поле <code>data</code>).
 * @see [jsonrpc.Peer]{@link https://bitbucket.org/sempasha/node-jsonrpc-sockjs-browser#main.Peer}
 */

/**
 * @class external:jsonrpc.Peer
 * @desc Класс, описывающий клиента JSON-RPC сервиса, подключенного к сервису по SockJS.
 * @see [jsonrpc.Peer]{@link https://bitbucket.org/sempasha/node-jsonrpc-sockjs-browser#main.Peer}
 */

/**
 * @method external:jsonrpc.Peer#_getRequestedProcedure
 * @desc Возвращает функцию соответствующую запрошенной процедуре. Для того, чтобы задать контекст выполнения процедуры
 * в ответ необходимо вернуть объект с двум полями <code>{function: Function, context: Object}</code>
 * @see [jsonrpc.Peer#_getRequestedProcedure]{@link https://bitbucket.org/sempasha/node-jsonrpc-sockjs-browser#main.Peer%23_getRequestedProcedure}
 */

/**
 * @fileOverview Клиент сервиса удалённого управления устройством дял weburg.tv. Является конечной реализацией,
 * не предполагает существование расширений данного клиента. Включает в себя два компонента:<ul>
 *     <li>[Peer]{@link module:main.Peer} - сам класс клиента Peer</li>
 *     <li>[Error]{@link module:main.Error} - класс для ошибок, возникающих по ходу выполнения процедур Error</li>
 * </ul>
 * @module main
 * @extends external:jsonrpc
 */

/**
 * Класс клиента
 *
 * @ignore
 * @alias module:main.Peer
 * @class
 */
exports.Peer = require('./lib/peer');

/**
 * Класс ошибок
 *
 * @ignore
 * @alias module:main.Error
 * @class
 */
exports.Error = require('./lib/error');