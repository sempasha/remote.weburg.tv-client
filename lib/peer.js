/**
 * @fileOverview Класс в котором описан клиент сервиса удалённого управления устройством для weburg.tv. Клиент ведёт
 * общение с сервисом по двунаправленному каналу (SockJS соединению), общение медлу клиентом и сервером происходит по
 * протоколу JSON-RPC 2.0. Клиент основан на библиотеке [jsonrpc]{@link external:jsonrpc} (а точнее на её классе
 * [jsonrpc.Peer]{@link external:jsonrpc.Peer})
 * @ignore
 */

/**
 * Событие говорит о том, что клиент прошел аутентификацию на сервере, процедура аутентификации запускается сразу после
 * того, как клиент установит соединение с сервисом, до тех пор, пока клиент не прошёл аутентификации он не должен
 * отправлять никаких запросов, кроме запроса на аутентификацию, тем не менее, клиент может отвечать на запросы сервиса.
 *
 * @event module:main.Peer#authenticate
 * @type {Undefined}
 */

/**
 * @ignore
 */
var inherits = require('util').inherits;

/**
 * @ignore
 */
var setArgsMap = require('node-jsonrpc-sockjs-browser').setArgsMap;

/**
 * @ignore
 */
var Peer = require('node-jsonrpc-sockjs-browser').Peer;

/**
 * @ignore
 */
var Request = require('node-jsonrpc-sockjs-browser').Request;

/**
 * Список параметров клиента по умолчанию, часть параметров унаследована от [jsonrpc.Peer~Options]
 *
 * @alias module:main.Peer~Options
 * @typedef
 * @augments {external:jsonrpc.Peer~Options}
 * @type Object
 * @property {Object} auth Параметр аутентификации на сервисе: тип клиента и уникальный
 * идентификатор клиента в рамках типа
 * @property {String} auth.type Тип клиента, см. [Peer#type]{@link module:main.Peer#type}
 * @property {String} auth.uid UID клиента, см. [Peer#uid]{@link module:main.Peer#uid}
 */
/* jshint ignore:start */
var Options = {
    auth: {
        type: null,
        uid: null
    }
};
/* jshint ignore:end */

/**
 * Клиент сервиса удалённого управления устройством
 *
 * @alias module:main.Peer
 * @constructor
 * @augments external:jsonrpc.Peer
 * @param {module:main.Peer~Options} options
 */
module.exports = exports = function (options) {
    if (!(this instanceof exports)) {
        return new exports(options);
    }

    var self = this, method;
    Peer.call(self, options);
    if (typeof options !== 'object' || options === null) {
        throw new TypeError('options must be a non-null object');
    }
    if (typeof options.auth !== 'object' || options.auth === null) {
        throw new TypeError('options.auth must be a non-null object');
    }
    if (typeof options.auth.type !== 'string') {
        throw new TypeError('options.auth.type is missing, type is required to authenticate client');
    }
    if (typeof options.auth.uid !== 'string') {
        throw new TypeError('options.auth.uid is missing, uid is required to authenticate client');
    }
    self._options.auth = {type: options.auth.type, uid: options.auth.uid};
    self.type = self._options.auth.type;
    self.uid = self._options.auth.uid;
    self.on('connect', function () {
        self.request('authenticate', {
            client: self._options.auth
        }).onFinish(function (error, result) {
            if (error) {
                self.emit('error', error);
                self._socket.close();
            } else {
                self.id = result;
                self.emit('authenticate');
                self.send();
            }
        });
    });
    self.on('disconnect', function () {
        self.id = null;
    });
    self._controller = {};
    for (method in exports.prototype._controller) {
        self._controller[method] = exports.prototype._controller[method];
    }
};

inherits(exports, Peer);

/**
 * Идентификатор клиента, определяется сервисом во время аутентификации. При успешной аутентификации сервер сообщает
 * клиенту его идентификатор. В свою очередь сервер проводит аутентификацию клиента по его
 * [типу]{@link module:main.Peer#type} и [уникальному id]{@link module:main.Peer#uid}. id клиента не известен до тех
 * пор, пока клиент не пройдёт аутентификацию, так же после того, как клиент закрывает (теряет) соединение с сервером
 * мы считаем, что его id так же неизвестен
 *
 * @alias module:main.Peer#id
 * @type {?String}
 */
exports.prototype.id = null;

/**
 * Тип клиента, типы клиента:<ul>
 *     <li><code>"weburg.tv/amino130"</code> для ТВ приставок Amino 130 на интерфейсе weburg.tv</li>
 *     <li><code>"weburg.tv/amino140"</code> для ТВ приставок Amino 139/140 на интерфейсе weburg.tv</li>
 *     <li><code>"weburg.tv/airties"</code> для ТВ приставок AirTies на интерфейсе weburg.tv</li>
 *     <li><code>"weburg.tv/mag250"</code> для ТВ приставок Mag 245/250 на интерфейсе weburg.tv</li>
 *     <li><code>"wmc.tv/windows"</code> для WMC.TV на Windows</li>
 *     <li><code>"wmc.tv/linux"</code> для WMC.TV на Linux</li>
 *     <li><code>"wmc.tv/macos"</code> для WMC.TV на Mac OS X</li>
 *     <li><code>"remote"</code> для приложения удалённого управлений устройством</li>
 *     <li><code>...</code> и т.д.</li>
 * </ul>
 * Пара значений [type]{@link module:main.Peer#type} и [uid]{@link module:main.Peer#uid} позволяют однозначно
 * идентифициорвать клиента, представивших эти значения
 *
 * @alias module:main.Peer#type
 * @type {String}
 */
exports.prototype.type = null;

/**
 * Уникальный идентификатор клиента в рамках типа клиента (auth.type). Пара значений
 * [type]{@link module:main.Peer#type} и [uid]{@link module:main.Peer#uid} позволяют однозначно идентифициорвать
 * клиента, представивших эти значения
 *
 * @alias module:main.Peer#uid
 * @type {String}
 */
exports.prototype.uid = null;

/**
 * Уникальный идентификатор клиента в рамках типа клиента (auth.type). Пара значений
 * [type]{@link module:main.Peer#type} и [uid]{@link module:main.Peer#uid} позволяют однозначно идентифициорвать
 * клиента, представивших эти значения
 *
 * @alias module:main.Peer#_controller
 * @private
 * @type {Object}
 */
exports.prototype._controller = {

    /**
     * Закрывает соединение со стороны клиента
     *
     * @returns {Undefined}
     */
    disconnect: setArgsMap(function () {
        var self = this;
        setTimeout(function () {
            self.disconnect();
        }, 0);
    }, []),

    /**
     * Отвечает на Ping запрос. Ответ состоит из значения, переданного в процедере в параметре payload.
     *
     * @param {*} payload
     * @returns {*}
     */
    ping: setArgsMap(function (payload) {
        return payload;
    }, ['payload'])
};

/**
 * Возвращает функцию соответствующую запрошенной процедуре. Для того, чтобы задать контекст выполнения процедуры в
 * ответ необходимо вернуть объект с двум полями <code>{function: Function, context: Object}</code>
 *
 * @alias module:main.Peer#_getRequestedProcedure
 * @protected
 * @override external:jsonrpc.Peer#_getRequestedProcedure
 * @param {String} method имя процедуры (см. [Request#method]{@link external:jsonrpc.Request#method})
 * @returns {(Function|{function: Function, context: Object})}
 */
exports.prototype._getRequestedProcedure = function (name) {
    if (typeof this._controller[name] !== 'undefined') {
        return {
            'function': this._controller[name],
            context: this
        };
    } else {
        return null;
    }
};

/**
 * Отправка сообщений, до того, как клиент пройдёт аутентификацию на серврее он может отправлять только запросы на
 * прохождение процедуры аутентификации, в противном случае сервер не примет запроса клиента и вернёт его с ошибкой
 * "Authentication required"
 *
 * @alias module:main.Peer#send
 * @returns {Undefined}
 */
exports.prototype.send = function () {
    if (this.id === null) {
        var send = [],
            do_not_send = [],
            message;

        while(this._sendBuffer.length > 0) {
            message = this._sendBuffer.shift();
            if (message instanceof Request && message.method === 'authenticate') {
                send.push(message);
            } else {
                do_not_send.push(message);
            }
        }
        this._sendBuffer = send;
        Peer.prototype.send.call(this);
        this._sendBuffer = this._sendBuffer.concat(do_not_send);
    } else {
        Peer.prototype.send.call(this);
    }
};

/**
 * Шорткат для отправки запроса удалённому клиенту, аналогично вызову
 * <code>peer.request('requestClient', {client_id: client_id, method: method, params: params})</code>.
 * Результат вызова может определить<ul>
 *     <li>сервер, отвечающий за пересылку запроса, - в случае, если у клиента отправляющего запрос нет прав на
 *     отправку запроса удалённому клиенту, либо при наступлении таймаута ожидания ответа от удалённого клиента</li>
 *     <li>удалённый клиент - в случае, когда запрос прошёл в нормальных условиях</li>
 * </ul>
 *
 * @alias module:main.Peer#requestClient
 * @param {String} client_id идентификатор клиента, которому необходимо отправить запрос
 * @param {String} method название метода, который будет вызван на удалённом клиенте
 * @param {?(Array|Object)} params список параметров вызова процедуры на удалённом клиенте
 * @returns {external:jsonrpc.Request} запрос вызова удалённой процедуры
 */
exports.prototype.requestClient = function (client_id, method, params) {
    return this.request('requestClient', {
        client_id: client_id,
        method: method,
        params: params
    });
};

/**
 * Шорткат для отправки запроса удалённому клиенту, аналогично вызову
 * <code>peer.request('requestClient', {client_id: client_id, method: method, params: params})</code>.
 * Результат вызова определяет ответ от сервера, пересылающего запрос. Результато может быть ошибка, либо в случае
 * удачной отправки уведомления - ничего (<code>null</code>, отсутствие ошибки). Ошибка отправки может возникнуть только
 * по причине отсутствия у клиента отправляющего запрос прав на отправку запросов удалённому клиенту.
 *
 * @alias module:main.Peer#notifyClient
 * @param {String} client_id идентификатор клиента, которому необходимо отправить запрос
 * @param {String} method название метода, который будет вызван на удалённом клиенте
 * @param {?(Array|Object)} params список параметров вызова процедуры на удалённом клиенте
 * @returns {external:jsonrpc.Request} запрос вызова удалённой процедуры
 */
exports.prototype.notifyClient = function (client_id, method, params) {
    return this.request('notifyClient', {
        client_id: client_id,
        method: method,
        params: params
    });
};

/**
 * Добавляет процедуру к контроллеру
 *
 * @alias module:main.Peer#addProcedure
 * @param {String} name имя процедуры
 * @param {Function} функция процедуры
 * @param {String[]} arguments_map описание параметров функции в виде списка имён аргументов
 * @returns {Undefined}
 * @example
 *
 *  var client = new Client(options);
 *  var procedure_ask_question = function (question) {
 *      return prompt(question);
 *  };
 *
 *  var procedure_ask_question_after_timeout = function (question, timeout) {
 *      var promise = createNewPromise();
 *      setTimeout(function () {
 *          promise.resolve(prompt(question));
 *      }, timeout);
 *      return promise;
 *  };
 *
 *  client.addProcedure('askQuestion', procedure_ask_question, ['question']);
 *  client.addProcedure('askQuestionAfterTimeout', procedure_ask_question_after_timeout, ['question', 'timeout']);
 */
exports.prototype.addProcedure = function (name, func, arguments_map) {
    if (typeof this._controller[name] !== 'undefined') {
        throw new Error('Procedure ' + name + ' already exists');
    }
    this._controller[name] = setArgsMap(func, arguments_map);
};

/**
 * Удаление процедуру из контроллера
 *
 * @alias module:main.Peer#removeProcedure
 * @param {String} name имя процедуры, которую необходимо удалить
 * @returns {Undefined}
 */
exports.prototype.removeProcedure = function (name) {
    delete this._controller[name];
};