/**
 * @fileOverview Класс, описывающий ошибки, произошедшие при выполнении процедур на клиенте.
 * @ignore
 */

/**
 * @ignore
 */
var inherits = require('util').inherits;

/**
 * @ignore
 */
var ErrorBasic = require('node-jsonrpc-sockjs-browser').Error;

/**
 * Клиент сервиса удалённого управления устройством
 *
 * @alias module:main.Error
 * @augments external:jsonrpc.Error
 * @constructor
 * @param {Number} code числовой код ошибки, положительное число, отрицательные коды зарезервированы и не могут
 * быть использованы для обозначения ошибок пользовательской логики
 * @param {String} message текстовое описание ошибки
 * @param {?(Boolean|Number|String|Object|Error)} [data=null] данные примитивного типа или структара данных,
 * содержащие дополнительную информацию об ошибке
 * @throws {TypeError} в случае, если переданы неверные значения для параметров <code>code</code> и <code>message</code>
 * @throws {RangeError} в случае, если задан недопустимый код ошибки
 */
module.exports = exports = function (code, message, data) {
    if (!(this instanceof exports)) {
        return new exports(code, message, data);
    }
    if (typeof code !== 'number' || (code % 1 !== 0)) {
        throw new TypeError('Invalid code, must be an positive integer, but ' +
            (typeof code) + ' (' + code + ') given');
    }
    if (code <= 0) {
        throw new TypeError('Invalid code, must be an positive integer, but ' + code + ' given');
    }
    ErrorBasic.call(this, code, message, data);
};

inherits(exports, ErrorBasic);