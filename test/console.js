/**
 * @fileOverview Добавление консоли для старых браузеров
 * @module console
 */

/* jshint browser: true */
var log,
    MAX_RECORDS = 10,
    colors = {
        '-': '#000000',
        '@': '#0000aa',
        '~': '#ae591f',
        '!': '#ff0000'
    },
    messages = [],
    write_before_load = function (level, message) {
        messages.push([level, message]);
    },
    write_after_load = function (level, message) {
        var records_count = log.children.length,
            item = document.createElement('PRE');

        item.innerHTML = level + ' ' + message;
        item.style.margin = 0;
        item.style.padding = 0;
        item.style.color = colors[level];
        log.insertBefore(item, records_count++ > 0 ? log.children[0] : null);
        if (records_count >= MAX_RECORDS) {
            log.removeChild(log.children[records_count - 1]);
        }
    },
    write = write_before_load,
    message = function () {
        var now = new Date(),
            h = now.getHours(),
            m = now.getMinutes(),
            s = now.getSeconds(),
            ms = now.getMilliseconds(),
            time = (h < 10 ? '0' + h : h) + ':' + (m < 10 ? '0' + m : m) + ':' + (s < 10 ? '0' + s : s) +
                '.' + ms + '000'.slice(-1 + ms.toString().length);
        return time + ' --> ' + Array.prototype.join.call(arguments, ' ').replace(/\n/g, '\n                    ');
    },
    onload = function () {
        var body = document.getElementsByTagName('BODY')[0];
        log = document.createElement('DIV');
        log.style.width = '100%';
        log.style.height = '100%';
        log.style.margin = 0;
        log.style.padding = 0;
        body.insertBefore(log, body.children.length > 0 ? body.children[0] : null);
        write = write_after_load;

        while(messages.length > 0) {
            write.apply(window, messages.shift());
        }
    };

if (document.readyState !== 'loading') {
    onload();
} else if (typeof window.addEventListener === 'function') {
    window.addEventListener('load', onload, false);
} else {
    window.onload = (function (current_onload) {
        return function () {
            if (current_onload) {
                current_onload.call(window);
            }
            onload.call(window);
        };
    })(typeof window.onload !== 'undefined' ? window.onload : null);
}

exports.debug = function () {
    write('-', message.apply(window, arguments));
};

exports.error = function () {
    write('!', message.apply(window, arguments));
};

exports.warn = function () {
    write('~', message.apply(window, arguments));
};

exports.info = function () {
    write('@', message.apply(window, arguments));
};

exports.log = function () {
    write('-', message.apply(window, arguments));
};