/**
 * @fileOverview Сервер для тестирования клиента сервиса адёлеггоно управления устройством
 * Использование:<ol>
 *     <li><code>$ node server.js start</code> - запуск сервера отдельным процессом</li>
 *     <li><code>$ node server.js stop</code> - остановка существущего процесса сервера</li>
 *     <li><code>$ node server.js</code> - запуск сервера в текущем процессе</li>
 * </ol>
 */

var path = require('path'),
    fs = require('fs'),
    working_directory = path.normalize(path.resolve(__dirname, '..')),
    server_module_file = path.resolve(__dirname, 'server.js'),
    server_pid_file = path.resolve(__dirname, 'server.pid');

main();

/**
 * Выполняет скрипт согласно заданным параметрам (см. описание файла)
 *
 * @returns {Undefined}
 */
function main () {
    try {
        var port_arg_index = process.argv.indexOf('--port'),
            help_arg_index = process.argv.indexOf('--help'),
            port = 9999;

        if (help_arg_index > -1) {
            console.log('');
            console.log('Usage: %s %s [options] action', process.argv[0], process.argv[1]);
            console.log('');
            console.log('options:');
            console.log('    --help  show this help message');
            console.log('    --port  number set port number, 9999 by default');
            console.log('');
            console.log('actions:');
            console.log('    start    to start server');
            console.log('    stop     to start server');
            console.log('    restart  to restart server');
            console.log('');
        } else {
            if (port_arg_index > -1) {
                port = parseInt(process.argv[port_arg_index + 1], 10);
                process.argv.splice(port_arg_index, 2);
            }
            switch (true) {
                case typeof process.argv[2] === 'undefined':
                    server(port);
                    break;

                case process.argv[2] === 'start':
                    start_server(port);
                    break;

                case process.argv[2] === 'stop':
                    stop_server();
                    break;

                case process.argv[2] === 'restart':
                    stop_server();
                    start_server();
                    break;
            }
        }
    } catch (error) {
        console.error(error);
    }
}

/**
 * Проверят состояние процесса тестового сервера, в случае, если он запущен вернёт PID процесса, если нет NULL
 *
 * @returns {?Number}
 */
function check_server () {
    var pid = null;
    if (fs.existsSync(server_pid_file)) {
        pid = fs.readFileSync(server_pid_file).toString();
        try {
            process.kill(pid, 0);
        } catch (error) {
            fs.unlinkSync(server_pid_file);
            pid = null;
        }
    }
    return pid;
}

/**
 * Запукает процесс тестового сервера
 *
 * @returns {undefined}
 * @throws Error в случае, если тестовый сервер уже запущен
 */
function start_server (port) {
    var file = process.argv[0],
        args = [server_module_file, '--port', port],
        opts = {
            cwd: working_directory,
            detached: true,
            stdio: ['ignore', 'ignore', 'ignore']
        },
        pid = check_server();

    if (pid !== null) {
        throw new Error('Server process already running, execute ' +
            '"' + process.argv[0] + ' ' + server_module_file + ' stop" to stop server');
    } else {
        require('child_process').spawn(file, args, opts).unref();
        console.log('start server at 0.0.0.0:' + port);
    }
}

/**
 * Останавливает запущенный процесс тестового сервера
 *
 * @returns {undefined}
 * @throws Error в случае, если нет запушенных процессов сервера
 */
function stop_server () {
    var pid = check_server();
    if (pid === null) {
        throw new Error('Server process is not running, execute ' +
            '"' + process.argv[0] + ' ' + server_module_file + ' start" to start server');
    } else {
        process.kill(fs.readFileSync(server_pid_file));
        fs.unlinkSync(server_pid_file);
    }
}

/**
 * Стартует тестовые сервер, тестовый сервер умеет:
 *  - обслуживать статические запросы:
 *    - index.html, index.js - отдаёт соответствующие файлы проекта
 *    - mocha.js, mocha.css - отдаёт файлы из модуля mocha
 *    - assert.js, index.js - создаёт bundle при помощи browserify и отправляет его клиенту
 *  - обслуживать SockJS подключения
 */
function server (port) {
    var browserify = require('browserify'),
        http = require('http'),
        http_server = http.createServer(),
        close_handler = function () {
            if (fs.existsSync(server_pid_file)) {
                fs.unlinkSync(server_pid_file);
            }
            process.exit();
        };

    fs.writeFileSync(server_pid_file, process.pid);
    process.on('exit', close_handler);
    process.on('SIGHUP', close_handler);
    process.on('SIGTERM', close_handler);
    process.on('SIGINT', close_handler);
    process.on('SIGBREAK', close_handler);
    process.on('uncaughtException', function (error) {
        console.error('uncaught error: %s', error && error.stack || error);
        close_handler();
    });

    http_server.on('request', function (req, res) {
        var url = require('url').parse(req.url),
            done = function (code, headers, body) {
                res.writeHead(code, headers);
                res.end(body);
            },
            files = {
                '/index.html': path.resolve(__dirname, 'index.html'),
                '/index.js': 'b:' + path.resolve(__dirname, 'index.js'),
                '/mocha.js': path.resolve(__dirname, '..', 'node_modules', 'mocha-phantomjs',
                    'node_modules', 'mocha', 'mocha.js'),
                '/mocha.css': path.resolve(__dirname, '..', 'node_modules', 'mocha-phantomjs',
                    'node_modules', 'mocha', 'mocha.css')
            },
            mime_types = {
                '.txt': 'text/plain; charset=utf-8',
                '.css': 'text/css; charset=utf-8',
                '.html': 'text/html; charset=utf-8',
                '.js': 'text/javascript; charset=utf-8'
            },
            send_browserify = function (files, standalone) {
                browserify(files)
                    .bundle({standalone: standalone}, function (error, result) {
                        if (error) {
                            done(500, {'Content-Type': mime_types['.txt']}, error.toString());
                        } else {
                            done(200, {'Content-Type': mime_types['.js']}, result);
                        }
                    });
            },
            send_file = function (file) {
                var ext = path.extname(file),
                    mime_type = typeof mime_types[ext] !== 'undefined' ? mime_types[ext] : 'test/plain';

                done(200, {'Content-Type': mime_type}, fs.readFileSync(file));
            };

        files[''] = files['/'] = files['/index.html'];

        if (url.pathname.indexOf('/service') !== 0) {
            if (typeof files[url.pathname] === 'undefined') {
                done(404, {'Content-Type': mime_types['.txt']}, 'Not found: ' + req.url);
            } else {
                if (files[url.pathname].indexOf('b:') === 0) {
                    send_browserify([files[url.pathname].slice(2)], url.pathname.slice(1, -3));
                } else {
                    send_file(files[url.pathname]);
                }
            }
        }
    });

    console.log('listen 0.0.0.0:' + port);
    http_server.listen(port, '0.0.0.0');
}