/**
 * @fileOverview Пакет тестов для модуля
 * @ignore
 */

/* global window */
if (typeof window === 'object' && typeof window.console !== 'undefined') {
    window.console = require('./console');
}
require('./peer');
require('./rpc.client');
require('./rpc.service');