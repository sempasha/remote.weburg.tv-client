/**
 * @fileOverview Тест клиента сервиса удалённого управления устройством для weburg.tv
 * @ignore
 */

/* global window, describe, it */
var assert = require('assert'),
    main = require('../'),
    url = window.location.protocol + '//172.16.67.37/connect/',
    auth = {type: 'test', uid: 'test'};

describe('main.Peer', function() {

    describe('#constructor', function () {
        it('Принимает список опций, обязательный параметр, options.url и options.auth - обязательные параметры',
            function () {
                var create = function (options) {
                    return function () {
                        return new main.Peer(options);
                    };
                };
                assert.throws(create(null), TypeError);
                assert.throws(create(''), TypeError);
                assert.throws(create({}), TypeError);
                assert.throws(create({auth: auth}), TypeError);
                assert.throws(create({url: url}), TypeError);
                assert.doesNotThrow(create({url: url, auth: auth}));
            });
    });

    describe('#_getRequestedProcedure', function () {
        it('По умолчанию для клиента определены методы: disconnect, ping',
            function () {
                var client = main.Peer({
                    url: url,
                    auth: auth
                });
                assert.ok(client._getRequestedProcedure('disconnect'), 'Метод disconnect не определён');
                assert.ok(client._getRequestedProcedure('ping'), 'Метод ping не определён');
            });

        it('Возвращает функцию, заданную с помощью Peer#addProcedure',
            function () {
                var client = main.Peer({
                        url: url,
                        auth: auth
                    }),
                    func = function (name) {
                        return 'Hello, ' + name;
                    };

                client.addProcedure('hello', func, ['name']);
                assert.strictEqual(client._getRequestedProcedure('hello')['function'], func, 'Получена не та функция,' +
                    ' которая ожидалась');
            });

        it('Все процедуры клиента имеют один контекст исполнения - это сам клиент',
            function () {
                var client = main.Peer({
                        url: url,
                        auth: auth
                    }),
                    procedures = ['disconnect', 'ping', 'hello'];

                client.addProcedure('hello', function (name) {return 'Hello, ' + name;}, ['name']);
                procedures.forEach(function (procedure_name) {
                    var procedure = client._getRequestedProcedure(procedure_name);
                    assert.ok(procedure instanceof Object, 'Контекст для процедуры ' + procedure_name + ' не задан');
                    assert.ok(procedure.context instanceof Object,
                        'Контекст для процедуры ' + procedure_name + ' не задан');
                    assert.strictEqual(procedure.context, client,
                        'Контекст для процедуры ' + procedure_name + ' задан неверно');
                });
            });
    });

    describe('#send', function () {
        it('Не отправляет любые запросы до тех пор, пока не пройдёт аутентификацию',
            function (done) {
                var client = main.Peer({
                        url: url,
                        auth: auth
                    }),
                    notifications_create = 10,
                    notifications_sent = 0,
                    notifications_created = 0,
                    create = function () {
                        ++notifications_created;
                        client.notify('ping').onFinish(function () {
                            assert.ok(connected, 'Клиент не установил соединение, но запрос был отправлен, как?');
                            assert.ok(authenticated, 'Клиент не прошел аутентификацию, но запрос был отправлен');
                            if (++notifications_sent === notifications_created) {
                                client.disconnect();
                                client.once('disconnect', function () {
                                    done();
                                });
                            }
                        });
                    },
                    connected = false,
                    authenticated = false;

                client.once('connect', function () {
                    connected = true;
                });

                client.once('authenticate', function () {
                    authenticated = true;
                });

                while (notifications_create-- > 0) {
                    create();
                }

                client.connect();
            });
    });

    describe('#requestClient', function () {
        it('Метод является оберткой аналогичной Peer#request("requestClient", {client_id: client_id, method: method, ' +
            'params: params})');
    });

    describe('#notifyClient', function () {
        it('Метод является оберткой аналогичной Peer#request("notifyClient", {client_id: client_id, method: method, ' +
            'params: params})');
    });

    describe('#addProcedure', function () {
        it('Добавляет процедуру, все параметры - обязательные',
            function () {
                var client = main.Peer({
                        url: url,
                        auth: auth
                    }),
                    func = function (name) {
                        return 'Hello, ' + name;
                    };

                client.addProcedure('hello', func, ['name']);
                assert.ok(client._getRequestedProcedure('hello'), 'Процедура не добавлена');
            });
    });

    describe('#removeProcedure', function () {
        it('Удаляет процедуру по заданному имени', function () {
            var client = main.Peer({
                    url: url,
                    auth: auth
                }),
                func = function (name) {
                    return 'Hello, ' + name;
                };

            client.addProcedure('hello', func, ['name']);
            assert.ok(client._getRequestedProcedure('hello'), 'Процедура не добавлена');

            client.removeProcedure('hello');
            assert.ok(!client._getRequestedProcedure('hello'), 'Процедура не удалена');
        });
    });
});