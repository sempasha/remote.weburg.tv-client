/**
 * @fileOverview Тест RPC интерфейса клиента
 * @ignore
 */

/* global describe, it */
var assert = require('assert'),
    main = require('../'),
    url = window.location.protocol + '//172.16.67.37/connect/',
    auth = {type: 'test', uid: 'test'};

describe('RPC интерфейс клиента', function() {
    describe('#ping', function () {
        it('Возвращает то же значение, что было передано в рараметре payload');
    });

    describe('#disconnect', function () {
        it('Инициирует отключение клиента от сервиса');
    });
});